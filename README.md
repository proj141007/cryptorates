# cryptorates

## Current architecture description
1) Once an hour scheduler app calls functions to collect data and update static website
2) Collector app collects data and puts in S3 bucket
3) Updater app updates static website on AWS S3
4) It has prod and dev environments

## How to run
1. Clone/Fork repositiories to your gitlab account
2. Clone locally infra repositiorin
3. Apply terraform manifest to build an environment
  - Be sure to use create and fill the terraform.tfvars file:
```sh
    private_key_path = "<path to ssh-key on local system>"
    inst_size = "t2.micro"
    key_name = "<ssh key name>"
    runner_token = "<token for registreing runner>"
    raw_data_bucket = "<where data from coinbase will be placed>"
    static_web_bucket = "<where static web-site will be placed>"
```
4. Make sure you corrected s3 buckets names for variablres in .gitlab-ci.yml
**RAW_DATA_BUCKET** - where data from coinbase will be placed
**STATIC_WEB_BUCKET** - where static web-site will be placed
5. Start pipeline from the gitlab console
6. Access static website using S3-link (or create a DNS CNAME record if you have a DNS zone)

## Infra improvements to be implemented
1. Install monitoring software (Prometheus+Grafana)
2. Install code tracing software (Sentry)
3. Redevelop terraform manifest for EKS infra or ECS infra
4. Develop helm charts for K8s orchestration if you plan to move to K8s

## Architecture improvements to be implemented
1. Improve granularity/frequency of data collection
2. Implement manual data collection and update methods
3. Move from static S3 to webserver (e.g. nginx-based service)
4. Create dynamic content (e.g React.js).
5. Add postgress db instead of S3 and add Redis for caching data