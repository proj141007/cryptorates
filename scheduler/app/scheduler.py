import requests
import schedule
import time
import os
import logging

COLLECTOR_API_URL = os.getenv(
    'COLLECTOR_API_URL', 'http://localhost:5000/collect')
UPDATER_API_URL = os.getenv('UPDATER_API_URL', 'http://localhost:5001/update')

logging.basicConfig(level=logging.INFO,
                    format='[%(asctime)s] %(levelname)s in %(module)s: %(message)s')


def call_collect_data_api():
    logging.info(
        f"Calling Collect Data API {COLLECTOR_API_URL}")
    response = requests.post(COLLECTOR_API_URL, json={})
    logging.info(
        f"Response: {response.status_code}, {response.text}")


def call_update_html_api():
    logging.info(
        f"Calling Update HTML API {UPDATER_API_URL}")
    response = requests.post(UPDATER_API_URL, json={})
    logging.info(
        f"Response: {response.status_code}, {response.text}")


# Schedule the API calls
# schedule.every().day.at("01:00").do(
#     call_collect_data_api)
schedule.every().hour.at(":10").do(
    call_collect_data_api)
# 30 minutes after data collection
# schedule.every().day.at("01:30").do(call_update_html_api)
schedule.every().hour.at(":15").do(call_update_html_api)

while True:
    schedule.run_pending()
    time.sleep(1)
