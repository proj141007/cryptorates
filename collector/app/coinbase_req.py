from flask import Flask, request, jsonify
import requests
import boto3
import json
from datetime import datetime
import logging
import os

DEFAULT_BUCKET_NAME = os.getenv('BACKET_NAME', 'coinb-rawdata-dev')

# logging
logging.basicConfig(level=logging.INFO,
                    format='[%(asctime)s] %(levelname)s in %(module)s: %(message)s')

app = Flask(__name__)


def fetch_coinbase_data():
    url = 'https://api.coinbase.com/v2/exchange-rates?currency=BTC'
    response = requests.get(url)

    if response.status_code == 200:
        logging.info("Successfully fetched data from Coinbase")
        return response.json()['data']['rates']
    else:
        logging.error(f"Error fetching data: {response.status_code}")
        return None


def upload_to_s3(data, bucket_name, file_name):
    s3 = boto3.client('s3')
    data_str = json.dumps(data)
    try:
        s3.put_object(Body=data_str, Bucket=bucket_name, Key=file_name)
        logging.info(
            f"{file_name} has been uploaded to the bucket {bucket_name}.")
    except Exception as e:
        logging.error(
            f"Failed to upload {file_name} to the bucket {bucket_name}. Error: {e}")


@app.route('/collect', methods=['POST'])
def collect_data():
    data = fetch_coinbase_data()
    if data:
        timestamp = datetime.now().strftime('%Y-%m-%d')
        file_name = f"coinbase_data_{timestamp}.json"
        bucket_name = request.json.get('bucket_name', DEFAULT_BUCKET_NAME)
        upload_to_s3(data, bucket_name, file_name)
        message = f"Data collected and uploaded as {file_name}"
        logging.info(message)
        return jsonify({"message": message}), 200
    else:
        error_message = "Failed to fetch data from coinbase"
        logging.error(error_message)
        return jsonify({"error": error_message}), 500
