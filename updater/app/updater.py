from flask import Flask, request, jsonify
import boto3
import json
from datetime import datetime
import os
from botocore.exceptions import ClientError
import logging

COLLECT_BUCKET = os.getenv('COLLECT_BUCKET', 'coinb-rawdata-dev')
TARGET_BUCKET = os.getenv('TARGET_BUCKET', 'coinbase-processed-data-dev')

logging.basicConfig(level=logging.INFO,
                    format='[%(asctime)s] %(levelname)s in %(module)s: %(message)s')

app = Flask(__name__)


def fetch_latest_data(bucket_name):
    s3 = boto3.client('s3', region_name='eu-central-1')
    current_date = datetime.now().strftime('%Y-%m-%d')
    file_name = f"coinbase_data_{current_date}.json"
    try:
        response = s3.get_object(Bucket=bucket_name, Key=file_name)
        data = json.loads(response['Body'].read())
        return data
    except ClientError as e:
        if e.response['Error']['Code'] == 'NoSuchKey':
            logging.info(
                f"File {file_name} not found in bucket {bucket_name}.")
        else:
            logging.error(
                f"Error fetching file {file_name} from bucket {bucket_name}: {e}")
            raise
    return None


def generate_html(data):
    """Generate an HTML string from data."""
    updated_at = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

    html = "<html><head><title>Exchange Rates</title></head><body>"
    html += f"<h1>Latest Exchange Rates for BTC (Updated: {updated_at})</h1><table>"
    for currency, rate in data.items():
        html += f"<tr><td>{currency}</td><td>{rate}</td></tr>"
    html += "</table></body></html>"
    return html


def upload_html_to_s3(html, bucket_name, file_name):
    # Upload the generated HTML file to S3
    s3 = boto3.client('s3')
    s3.put_object(Body=html, Bucket=bucket_name, Key=file_name)
    print(f"Uploaded {file_name} to bucket {bucket_name}.")
    try:
        s3.put_object(Body=html, Bucket=bucket_name,
                      Key=file_name, ContentType='text/html')
        logging.info(f"Uploaded {file_name} to bucket {bucket_name}.")
    except ClientError as e:
        logging.error(
            f"Failed to upload {file_name} to bucket {bucket_name}: {e}")


@app.route('/update', methods=['POST'])
def update_html():
    bucket_name = COLLECT_BUCKET
    data = fetch_latest_data(bucket_name)
    file_name = request.json.get('file_name', 'index.html')
    if data:
        html_content = generate_html(data)
        upload_html_to_s3(html_content, TARGET_BUCKET, file_name)
        message = f"HTML updated successfully and uploaded as {file_name} to {TARGET_BUCKET}."
        logging.info(message)
        return jsonify({"message": message}), 200
    else:
        error_message = "Failed to collect data from bucket"
        logging.error(error_message)
        return jsonify({"error": error_message}), 500
